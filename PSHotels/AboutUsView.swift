//
//  AboutUsView.swift
//  PSHotels
//
//  Created by Panacea-Soft on 10/26/17.
//  Copyright © 2018 Panacea-Soft ( www.panacea-soft.com ). All rights reserved.
//

import UIKit

@IBDesignable
class AboutUsView: PSUIView {

    // MARK: Custom Variables
    let aboutUsViewModel = AboutUsViewModel()
    
    @IBOutlet weak var aboutUsImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!

    
    
    
    // MARK: - Override Functions
    override func initVariables() {
        aboutUsViewModel.nibName = "AboutUsView"
    }
    
    // MARK: - Override Functions
    // joining AboutUsView and AboutUsView.xib
    override func getNibName() -> String {
        return aboutUsViewModel.nibName
    }
    
    override func initUIViewAndActions() {
        super.initUIViewAndActions()
        
        // Loading Monitoring
        aboutUsViewModel.isLoading.bind{
            if($0) {
                Common.instance.showBarLoading()
            }else {
                Common.instance.hideBarLoading()
            }
        }
        
        setupFont()
        setupLabelTitle()
        setupColor()
    }
    
    override func initData() {
        // Load data from PS Server
        
        aboutUsViewModel.getAboutUs()
        aboutUsViewModel.aboutUsLiveData.bind{ [weak self] in
            
            if let resourse : Resourse<AboutUs> = $0 {
                
                if resourse.status == Status.SUCCESS
                    || resourse.status == Status.LOADING {
                    
                    if let aboutUs : AboutUs = resourse.data {
                        
                        self?.bindAboutUsData(aboutUs)
                        
                    }
                    
                } else if resourse.status == Status.ERROR {
                    print("Error in loading data. Message : " + resourse.message)
                }
            }else {
                print("Something Wrong")
            }
            
        }
        
    }
    
    
    func setupFont() {
        titleLabel.font = customFont.headerUIFont
        descLabel.font = customFont.normalUIFont
    }
    
    //MARKS: Custom Functions
    func setupLabelTitle() {
        
    }
    
    func setupColor() {
        
        let textColor = configs.colorText
        
        //Text Data
        titleLabel.textColor = textColor
        descLabel.textColor = textColor
        
        
        //Title

        
    }
    
    func bindAboutUsData(_ aboutUs: AboutUs) {
        titleLabel.text = aboutUs.about_title
        titleLabel.setLineHeight(height: configs.lineSpacing,aligment: .center)
        descLabel.text = aboutUs.about_description
        descLabel.setLineHeight(height: configs.lineSpacing,aligment: .center)

        
        let imageURL = configs.imageUrl + aboutUs.default_photo.img_path
        
        aboutUsImage.sd_setImage(with: URL(string: imageURL), placeholderImage: UIImage(named: "PlaceholderImage"))
        
    }
    
}
